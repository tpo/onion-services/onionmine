# Alternatives

Other implementations than the [mkp224o generator][]:

* [ciehanski/oniongen-hs: v3 onion vanity URL generator written in Haskell](https://github.com/ciehanski/oniongen-hs)
* [rdkr/oniongen-go: 🔑 v3 .onion vanity URL generator written in Go](https://github.com/rdkr/oniongen-go)
* [oniongen-rs: a v3 .onion vanity URL generator written in Rust](https://gitlab.com/iamawacko-oss/oniongen-rs)

Other implementations than [onion-csr][]:

* [onion-csr implementation in Go](https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/onion-csr).

[mkp224o generator]: https://github.com/cathugger/mkp224o
[onion-csr]: https://github.com/HARICA-official/onion-csr
