# References

* [Vanity Addresses](https://community.torproject.org/onion-services/advanced/vanity-addresses/) at the [Tor Community Portal](https://community.torproject.org).
* [Tips when mining Onion Addresses](https://tpo.pages.torproject.net/onion-services/onionspray/guides/mining/).
  contains important advice.
* The guide [Make Your Site Available Over Tor: Guide To EOTK, The Enterprise Onion Toolkit](https://shen.hong.io/making-websites-on-tor-using-eotk/)
  has a detailed discussion about how to mine keys using `mkp224o`.
* [mkp224o/OPTIMISATION.txt at master · cathugger/mkp224o](https://github.com/cathugger/mkp224o/blob/master/OPTIMISATION.txt)
  details the optimization flags of `mkp224o`.
* The [VANITY][] and [VANITY-REFS][] sections from the
  [Tor Rendezvous Specification - Version 3][].
* The Part three of [Facebook, hidden services, and https certs | The Tor Project](https://blog.torproject.org/facebook-hidden-services-and-https-certs/) has
  and interesting discussion brute forcing the .onion keyspace.

[VANITY]: https://spec.torproject.org/rend-spec/vanity-onions.html#appendix-c-recommendations-for-searching-for-vanity-onions-vanity
[VANITY-REFS]: https://spec.torproject.org/rend-spec/references.html?highlight=vanity-refs#references
[Tor Rendezvous Specification - Version 3]: https://spec.torproject.org/rend-spec/
