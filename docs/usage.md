# Usage

## Getting

Get Onionmine with it's submodules directly from the [repository][] using

    git clone --recursive https://gitlab.torproject.org/tpo/onion-services/onionmine.git

[repository]: https://gitlab.torproject.org/tpo/onion-services/onionmine

## Configuring

Onionmine uses an `onionmine.conf` file to set it's main configuration,
including `mkp224o` build and invocation flags. Onionmine provides a [sample
configuration][] with lots of examples:

    cp onionmine.conf.sample onionmine.conf # edit to customize

[sample configuration]: https://gitlab.torproject.org/tpo/onion-services/onionmine/-/blob/main/onionmine.conf.sample

## Provisioning

The following command installs the needed dependencies on an Debian-compatible
system and compiles `mkp224o` (requires `sudo` privileges):

    ./onionmine provision

You might run it on a dedicated enviroment, such as an Raspberry Pi mini-server
or a dedicated virtual machine.

Currently testes only on [Debian GNU/Linux](https://www.debian.org).

## Mining

Create a configuration, use your favourite `$EDITOR` to create filters
and start mining!

    ./onionmine config my-pool # create a config called "my-pool"
    ./onionmine edit   my-pool # populate your filters
    ./onionmine mine   my-pool # start mining

This command takes care of compiling `mkp224o` and start mining with the
desired configuration.

Run the above `onionmine config` command for each Onion Service you're mining
keys.  Configuration and mined keys are stored under the `pool/my-pool` folder,
with mined keys at the `pool/my-pool/candidates`.

Check [this configuration][] for an example mining pool.

[this configuration]: https://gitlab.torproject.org/tpo/onion-services/onionmine/-/tree/main/pools/example.org

## Per-pool environment override

Custom environment overrides are possible. For the `my-pool` config, place any
overrides at the `pools/my-pool/pool.conf` or `pools/my-pool/local.conf` file,
which are parsed after the default `onionmine.conf` folder.

## Recompiling

If by any change a `mkp224o` recompilation is needed without triggering
additional key generation, use this command (e.g. after you change it's build
flags at the `onionmine.conf` file):

    ./onionmine compile

## Stopping and resuming

The [onionmine.conf.sample][] commes with examples to
handle the `mkp224o` seed passphrase option to be used along the `--checkpoint`
parameter if you plan to stop/resume the search.

Stopping can be done by interrupting the mining operation (like typing `Ctrl C`)
and resuming happens once you re-start mining with the same parameters as
previously.

If you get this error:

    ERROR: could not create directory for key output

That's probably because you're resuming a previous mining for a given pool
and `mkp224o` tried to create a folder for a keypair candidate that already
exists. Just let `mkp224o` continue to run and in a while you should get
new keys again.

Please note the security implications of this feature. As `mkp224o` outputs
when using `-P`:

> CAUTION: avoid using keys generated with same password for unrelated
> services, as single leaked key may help attacker to regenerate related keys.

That means that passphrases should:

* Not be reused for different services.
* Either be destroyed along unused candidates OR, if stored, have the same
  storage security level than the .onion keypair.

[onionmine.conf.sample]: https://gitlab.torproject.org/tpo/onion-services/onionmine/-/blob/main/onionmine.conf.sample

## Onionbalance compatibility

When generating keys for [Onionbalance][], make sure to use one pool per key:

1. One pool for the frontend instance.
2. One pool per backend instance.

This is for security considerations: keys generated within the same pool are highly
correlated. This means that if an attacker takes control of a backend instance that
uses a keypair generated in the same pool, then the attacker can try to regenerate
related keys.

[Onionbalance]: https://onionservices.torproject.org/apps/base/onionbalance/

## Testing your candidate keypairs

Once (or before) you select a candidate address that suits your needs, you
might want to test if the keys are validated by the Tor process, just to make
sure.

Onionmine provides a test script for that:

    ./onionmine test-candidate

Check [this
issue](https://gitlab.torproject.org/tpo/onion-services/onionmine/-/issues/2)
to see how it works.

## Selecting a candidate

It's possible to indicate that a candidate is selected by running the
`selected-candidate` sub-command like this:

    ./onionmine select-candidate example.org test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

This only creates a symbolic link in the pool folder to make easy to know which
candidate is currently selected.

## HTTPS certificates compatibility

Onionmine ships with [onion-csr][] as a submodule to ease the creation of
certificates for HTTPS usage with a selected candidate from a given pool.

The [provisioning][] procedure also makes sure that this software is compiled.

It can be invoked by running

    ./onionmine onion-csr <pool> <nounce>

[provisioning]: https://gitlab.torproject.org/tpo/onion-services/onionmine/-/blob/main/bin/provision
[onion-csr]: https://github.com/HARICA-official/onion-csr

## Encrypting selected keys to an external keystore

It's possible to encrypt the selecte keys to an external keystore of your choice,
by using the following command:

    ./onionmine encrypt-selected-key <pool>

It only needs that you setup a custom encryption command using the
`ENCRYPTION_COMMAND` param at your `onionmine.conf`. Check the [sample
configuration][] for details.

## Decrypting selected keys from an external keystore

Similarly, an decryption command is provided for Onion Service keys, restoring
then from a keystore to the mining pool:

    ./onionmine decrypt-key <pool> <onion-address>

Certificates for selected keys can also be decrypted easily:

    ./onionmine decrypt-selected-cert <pool> <onion-address>

## Remote mining

Onionmine provides two helper subcommands to sync between local and remote hosts:

    ./onionmine sync-to-remotes   # syncs both the codebase and the pools to remote hosts
    ./onionmine sync-from-remotes # syncs only the pools from remote hosts

Remote hosts and base folder can be set using the `onionmine.conf` config file, as well as
other parameters such as `rsync` options and exclusion patterns.

This allows remote mining operations, like in a server farm.

## Batch operations

Some operations have a batch mode, like this:

    ./onionmine test-keys-batch <batch_name|all>

Batches reside at the `batches/<batch_name>` folders with
the following files there:

* `batch.conf`: an optional file to allow configuration parameter
  from `onionmine.conf` to be overriden.

* `batch.list`: the list of actions to be performed: each line
  can contain only the pool name or also space-separated
  options for each invocation.

* A per-operation folder, like `batches/<batch_name>/test-keys`,
  hosting operation data such as:
  * `status`: tracks the latest status of a given batch.
  * `processed`: stores all the processed tasks in an operation.

To run a batch for all the pools, use the `all` special batch name,
corresponding to the `batches/all` folder. In this case, `batch.list`
and `batch.conf` are ignored.

## Wiping secrets and pools

Once you have deployed the service (or at least have all keys storage in an encrypted
long-term storage), you can wipe the pool secrets using this command:

    ./onionmine wipe-pool-secrets <domain>

The command above only removes secret material such as private keys,
passphrases and revocation codes.

If you want to permanently delete the whole mining pool, including non-private
material such as public keys, use this command instead:

    ./onionmine wipe-pool <domain>

In any case, make sure first to encrypt selected material in a long-term
encrypted keystore.

## Other commands

For the full list of available commands, type

    ./onionmine
