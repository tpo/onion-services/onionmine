# Example vanity pool

This folder contains a sample configuration to mine keys
for an hypothetical example.org site:

* `filters.lst`: the vanity filter list; the example
  provided contains regular expressions, but you don't
  have to use those.

* `local.conf`: local configuration overrides.
