#!/usr/bin/env bash
#
# Edit an Onionmine pool.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
source $DIRNAME/../lib/params

# Check
if [ -z "$CONFIG" ]; then
  echo "usage: onionmine $BASENAME <config-pool-name>"
  exit 1
elif echo "$CONFIG" | grep -q " "; then
  echo "$BASENAME: pool name cannot contain spaces"
  exit 1
fi

# Create config folder and filter list
mkdir -p $POOL
touch $FILTERS

# Edit filters
if [ ! -z "$EDITOR" ]; then
  echo "$BASENAME: opening $FILTERS with $EDITOR..."
  $EDITOR $FILTERS
else
  echo "$BASENAME: please set your EDITOR environment variable first"
  exit 1
fi
