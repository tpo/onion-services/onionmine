#!/usr/bin/env bash
#
# Wipe a mining pool.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
source $DIRNAME/../lib/params

# Additional parameters
CONFIRMATION_CODE="`shuf -i 10000-99999 -n 1`"

function usage() {
  echo "usage: onionmine $BASENAME <pool>"
  echo ""
  echo "Wipe a mining pool"
  echo ""
  echo "    pool:  the mining pool to operate on"
}

# Check for a config
if [ -z "$CONFIG" ]; then
  usage
  echo ""
  echo "Available pools:"
  echo ""
  ls -1 $POOLS | sed -e 's/^/    /'
  echo ""
  exit 1
fi

# Check if pool exists
if [ ! -e "$POOL" ]; then
  echo "$BASENAME: pool not found: $POOL"
  exit 1
fi

# Check for wiping tool
if ! which wipe &> /dev/null; then
  echo "$BASENAME: please install the 'wipe' tool"
  exit 1
fi

# Get a confirmation
echo "This operation is irreversible if you don't have backups for all secret material inside pool $CONFIG. Are you sure you want to proceed?"
read -p "Please type the confirmation code $CONFIRMATION_CODE: " confirm

# Confirm the operation
if [ "$confirm" != "$CONFIRMATION_CODE" ]; then
  echo "Confirmation code does not match"
  exit 1
fi

# Dispatch
wipe -r $POOL
exit $?
