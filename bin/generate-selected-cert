#!/usr/bin/env bash
#
# Generate a TLS keypair and CSR.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
source $DIRNAME/../lib/params

# Additional parameters
OUTPUT="$POOL/certs/selected"
PASSWORD_SIZE="`shuf -i 20-50 -n 1`"

function usage() {
  echo "usage: onionmine $BASENAME <pool>"
  echo ""
  echo "Generate a TLS keypair and a CSR for HTTPS connections for the selected candidate"
  echo ""
  echo "    pool:  the mining pool to operate on"
}

# Check for a config
if [ -z "$CONFIG" ]; then
  usage
  echo ""
  echo "Available pools:"
  echo ""
  ls -1 $POOLS | sed -e 's/^/    /'
  echo ""
  exit 1
fi

# Check if pool exists
if [ ! -e "$POOL" ]; then
  echo "$BASENAME: pool not found: $POOL"
  exit 1
fi

# Check if a selected candidate exists
if [ ! -e "$POOL/selected" ]; then
  echo "$BASENAME: please select a candidate first"
  exit 1
fi

# Check if OpenSSL is available
if ! which openssl &> /dev/null; then
  echo "$BASENAME: please install OpenSSL"
  exit 1
fi

# Check for an existing private key
if [ -e "$OUTPUT/privatekey.pem" ]; then
  REUSE_KEYS="y"

  if [ "$ONIONMINE_INTERACTIVE" != "n" ]; then
    echo "$BASENAME: private key $OUTPUT/privatekey.pem already exists"
    read -p "Do you want to reuse it? (y/n) " REUSE_KEYS
  else
    echo "$BASENAME: private key $OUTPUT/privatekey.pem already exists, skipping"
  fi

  if [ "$REUSE_KEYS" == "y" ]; then
    echo "CSR is:"
    cat "$OUTPUT/csr.pem"
    exit
  fi
fi

# Ensure that the output folder exists
mkdir -p -m 700 $OUTPUT || exit 1

# Get the selected address
SELECTED="$(basename `readlink $POOL/selected`)"

# Create an OpenSSL configuration
cat <<EOF >> $OUTPUT/openssl.conf
[ req ]
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
commonName_default     = $SELECTED
organizationName       = .
organizationalUnitName = .
emailAddress           = .
localityName           = .
stateOrProvinceName    = .
countryName            = .
commonName             = .
EOF

# Generate a passphrase
touch     $OUTPUT/passphrase
chmod 600 $OUTPUT/passphrase || exit 1
head -c $PASSWORD_SIZE /dev/urandom | base64 > $OUTPUT/passphrase

# Generate the keypair and the CSR
openssl req -batch -config $OUTPUT/openssl.conf -newkey ec -pkeyopt ec_paramgen_curve:secp384r1 \
            -keyout $OUTPUT/privatekey.pem -out $OUTPUT/csr.pem -sha384 -passout file:$OUTPUT/passphrase

# Save an unencrypted copy of the private key
#openssl ec -in $OUTPUT/privatekey.pem -out $OUTPUT/key.pem -passin file:$OUTPUT/passphrase

# Confirm
openssl req -in $OUTPUT/csr.pem -text

# Teardown
rm -f $OUTPUT/openssl.conf
exit $?
