#!/usr/bin/env bash
#
# Unwrap a TLS private key.
#
# Copyright (C) 2023 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
source $DIRNAME/../lib/params

# Additional parameters
OUTPUT="$POOL/certs/selected"

function usage() {
  echo "usage: onionmine $BASENAME <pool>"
  echo ""
  echo "Unwrap the selected TLS private key by writing it to a file without encryption"
  echo ""
  echo "    pool: the mining pool to operate on"
}

# Check for a config
if [ -z "$CONFIG" ]; then
  usage
  echo ""
  echo "Available pools:"
  echo ""
  ls -1 $POOLS | sed -e 's/^/    /'
  echo ""
  exit 1
fi

# Check if pool exists
if [ ! -e "$POOL" ]; then
  echo "$BASENAME: pool not found: $POOL"
  exit 1
fi

# Check if a selected candidate exists
if [ ! -e "$POOL/selected" ]; then
  echo "$BASENAME: please select a candidate first"
  exit 1
fi

# Check if OpenSSL is available
if ! which openssl &> /dev/null; then
  echo "$BASENAME: please install OpenSSL"
  exit 1
fi

# Check for an existing private key
if [ ! -e "$OUTPUT/privatekey.pem" ]; then
  echo "$BASENAME: certificate $OUTPUT/privatekey.pem doest not exists, please create it first"
  exit 1
fi

# Check if the passphrase file exists
if [ ! -e "$OUTPUT/passphrase" ]; then
  echo "$BASENAME: passphrase file $OUTPUT/passphrase does not exists"
  exit 1
fi

# Write the unencrypted key
openssl ec -in $OUTPUT/privatekey.pem \
           -out $OUTPUT/key.pem \
           -passin file:$OUTPUT/passphrase
