#!/usr/bin/env bash
#
# Decrypt a candidate into a given pool.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
source $DIRNAME/../lib/params

# Additional parameters
ONION_ADDR="$2"
MATERIALS="hs_ed25519_public_key hs_ed25519_secret_key"

# Check
if [ -z "$CONFIG" ] || [ -z "$ONION_ADDR" ]; then
  echo "usage: onionmine $BASENAME <pool> <onion-address>"
  echo ""
  echo "Available pools:"
  echo ""
  ls -1 $POOLS | sed -e 's/^/    /'
  echo ""
  exit 1
elif [ -z "$DECRYPTION_COMMAND" ]; then
  echo "$BASENAME: please set the DECRYPTION_COMMAND in your config"
  exit 1
fi

# Ensure that the candidate folder exists
mkdir -p $POOL/candidates/$ONION_ADDR

# Set the hostname
echo $ONION_ADDR > $POOL/candidates/$ONION_ADDR/hostname

# Decrypt
for MATERIAL in $MATERIALS; do
  touch     $POOL/candidates/$ONION_ADDR/$MATERIAL
  chmod 600 $POOL/candidates/$ONION_ADDR/$MATERIAL

  eval $DECRYPTION_COMMAND
done
